UTC Salt Sensor
PAI18-016

This repository is for the text-based pieces of Rockwell logic as well as the binaries/installation files/backups.

The repository contains copies of the source code external to the RSLogix editor (as Rockwell stores its projects as one giant BLOB (Binary Large OBject) rather than discrete files)
